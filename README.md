# Tsyringe Express Decorators

This package provide some simple decorators to declare ExpressJs controller classes and resolve controllers dependencies with Microsoft Tsyringe dependency injection container.

## Dependencies
- ExpressJs (https://www.npmjs.com/package/express)
- Tsyringe (https://www.npmjs.com/package/tsyringe)
- Reflect-metadata (https://www.npmjs.com/package/reflect-metadata)

*the package reflect-metadata should be imported in the project entrypoint.*

## Example

```ts
import "reflect-metadata";
import { Controller, Action, HttpMethod, attachController } from "tsyringe-express";
import { container, injectable } from "tsyringe";
import * as express from "express";

/**
 * Simple class that provide html to controller
 */
class HtmlProvider {

    public getHtml(): string {
        return "<html><body><h1>It works!</h1></body></html>";
    }

}

/**
 * Simple controller, that listen on route '/examples'
 * Depends on HtmlProvider to render html
 */
@Controller({ route: "/examples" })
@injectable()
class ExampleController {

    /**
     * Constructor with dependencies
     */
    constructor(private htmlProvider: HtmlProvider) {}
    
    /**
     * itworks action, listen on route '/examples/itworks'
     * Should have express Request and Response as parameters
     */
    @Action({
        route: "/itworks",
        method: HttpMethod.GET,
        middlewares: [
            function (req, res, next) {
                console.log("Middleware activated!");
                next();
            }
        ]
    })
    public itWorksAction(req: express.Request, res: express.Response) {
        res.status(200).send( this.htmlProvider.getHtml());
    }
}

const app = express();
attachController(app, container, [ ExampleController ]);

app.listen(3000);
```

