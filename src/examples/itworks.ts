import "reflect-metadata";
import { Controller, Action, HttpMethod, attachController } from "../index";
import * as express from "express";
import { container, injectable } from "tsyringe";


/**
 * Simple class that provide html to controller
 */
class HtmlProvider {

    public getHtml(): string {
        return "<html><body><h1>It works!</h1></body></html>";
    }

}

/**
 * Simple controller, that listen on route '/examples'
 * Depends on HtmlProvider to render html
 */
@Controller({ route: "/examples" })
@injectable() // tsyringe decorator
class ExampleController {

    /**
     * Constructor with dependencies
     */
    constructor(private htmlProvider: HtmlProvider) {}
    
    /**
     * itworks action, listen on route '/examples/itworks'
     * Should have express Request and Response as parameters
     */
    @Action({
        route: "/itworks",
        method: HttpMethod.GET,
        middlewares: [
            function (req, res, next) {
                console.log("Middleware activated!");
                next();
            }
        ]
    })
    public itWorksAction(req: express.Request, res: express.Response) {
        res.status(200).send( this.htmlProvider.getHtml());
    }
}

const app = express();
attachController(app, container);
app.listen(3000);
